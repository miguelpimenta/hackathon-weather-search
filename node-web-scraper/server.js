var express = require('express');
var fs      = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('points.csv')
});


lineReader.on('line', function (line) {
  var split = line.split(',');
  console.log(split);

  var url = 'http://www.worldweatheronline.com/v2/weather.aspx?q=' + split[1] + ',' + split[2] + '&day=20';


  request(url, function(error, response, html){
    if(!error){
      var $ = cheerio.load(html);

      var geoLocation = {};
      geoLocation.id = split[0];
      geoLocation.latitude = split[1];
      geoLocation.longitude = split[2];
      geoLocation.forecasts = [];
      geoLocation.url = url;


      for (i = 0; i < 14; i++) {
        var forecast = {};

        var date, temperature, condition;

        date = $('.loc-outlook-heading').eq(i).text();
        forecast.date = date.replace('st', '').replace('nd', '').replace('rd', '').replace('th', '');;

        temperature = $('.temperature').eq(i).text();
        forecast.temperature = temperature.substring(temperature.indexOf(" ")+1,temperature.indexOf("°"));

        if (i < 7) {
          condition = $('th:contains("Weather")').eq(i).next('td').next('td').next('td').next('td').next('td').find('img').attr('title');
        } else {
          condition = $('th:contains("Weather")').eq(i).next('td').next('td').next('td').find('img').attr('title');
        }
        forecast.condition = condition;

        geoLocation.forecasts[i] = forecast;
      }
    }
    fs.writeFile('output/' + geoLocation.id + '.json', JSON.stringify(geoLocation, null, 4), function(err){
      console.log('File successfully written!');
    })
    console.log(geoLocation);
  })
});
