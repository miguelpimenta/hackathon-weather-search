$(document).ready(function(){
   $.ajax({
      url: 'http://localhost:8080/getMatchingCoordinates?date=2016-09-15&weatherConditionList=CLEAR_SKIES&maxTemp=30&minTemp=15',
      // data: {
      //    format: 'json'
      // },
      error: function() {
        console.log("ERROR!");
      },
      success: function(data) {
          var myLatLng = {lat: 43.53610, lng: 9.66797};

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5,
            center: myLatLng
          });

        for (i = 0; i < data.length; i++) { 
            var current = {lat: parseFloat(data[i].lat), lng: parseFloat(data[i].lon)};
            var marker = new google.maps.Marker({
              position: current,
              map: map
            });
          }
          console.log(data);
      },
      type: 'GET'
   });
});


